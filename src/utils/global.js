export function jsonStringify(obj) {
    // convert object into a string preserving undefined/null values

    if (obj == undefined) {
        return undefined
    }

    return JSON.stringify(
        obj,
        function(k, v) { if (v === undefined) { return null } return v }
    )
}

export function jsonParse(obj) {
    // convert string/object into json object if possible

    if (obj == undefined) {
        return undefined
    }

    if (typeof obj == 'string') {
        try {
            return JSON.parse(obj)
        } catch (e) {
            // console.log('error',e)
            return obj
        }
    } else {
        return obj
    }
}

export function formatSizeUnits(bytes) {
    if      (bytes >= 1073741824) { return (bytes / 1073741824).toFixed(2) + ' GB' }
    else if (bytes >= 1048576)    { return (bytes / 1048576).toFixed(2) + ' MB' }
    else if (bytes >= 1024)       { return (bytes / 1024).toFixed(2) + ' KB' }
    else if (bytes > 1)           { return bytes + ' bytes' }
    else if (bytes == 1)          { return bytes + ' byte' }
    else                          { return '0 bytes' }
}

export function byteCount(str){
    if (str != undefined) {
        return encodeURI(str).split(/%..|./).length - 1
    } else {
        return 0
    }
}

export function getStringSizeUnit(str) {
    return formatSizeUnits(byteCount(str))
}

