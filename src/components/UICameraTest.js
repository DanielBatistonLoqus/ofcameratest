import { Modal, StyleSheet, View, InteractionManager, Button } from 'react-native'
import { observable } from 'mobx'
import { observer } from 'mobx-react/native'
import { RNCamera } from 'react-native-camera'
import React, { Component } from 'react'

import {jsonStringify} from '../utils/global'
import DebugHelper from '../utils/DebugHelper'

@observer
export default class UICameraTest extends Component {
    @observable visible = false
    @observable isCameraReady = false
    @observable isTakingPicture = false

    constructor(props) {
        super(props)

        this.cameraComponentObj = undefined
        this.visible = false

        this.updateProps(props)
    }

    UNSAFE_componentWillReceiveProps(props) {
        this.updateProps(props)
    }

    componentWillUnmount() {
        this.destroyCameraComponent()
    }

    // -------------------------------------------------------------------------------

    updateProps = (props) => {
        this.visible = props.visible
        this.onCapture = props.onCapture
        this.onCancel = props.onCancel

        if (this.visible == true) {
            this.createCameraComponent()
        }
    }

    // -------------------------------------------------------------------------------

    createCameraComponent = () => {
        if (this.cameraComponentObj == undefined) {
            console.debug('UICamera - Create')

            // Passing null to onFaceDetected, onGoogleVisionBarcodesDetected, onTextRecognized, onBarCodeRead automatically turns off the correspondent detector.
            this.cameraComponentObj = <RNCamera
                ref={(cam) => {
                    this.cameraComponentRef = cam
                }}
                style={styles.preview}
                // CAMERA Settings
                autoFocus={RNCamera.Constants.AutoFocus.on}
                type={RNCamera.Constants.Type.back}
                flashMode={RNCamera.Constants.FlashMode.auto}
                whiteBalance={RNCamera.Constants.WhiteBalance.auto}
                ratio={'4:3'}
                pictureSize={'640x480'}

                // CALLBACKS
                onCameraReady={() => {
                    setTimeout(() => {
                        this.isCameraReady = true
                        console.debug('UICamera - Camera ready')
                    }, 500)
                }}
                onMountError={(error) => {
                    console.debug('UICamera - Error to mount camera', error)
                }}
                androidCameraPermissionOptions={{
                    title: 'Permission to use camera',
                    message: 'We need your permission to use your camera',
                    buttonPositive: 'Ok',
                    buttonNegative: 'Cancel',
                }}
                androidRecordAudioPermissionOptions={{
                    title: 'Permission to use audio recording',
                    message: 'We need your permission to use your audio',
                    buttonPositive: 'Ok',
                    buttonNegative: 'Cancel',
                }}

                // AUDIO // all disabled
                onAudioInterrupted={null}
                onAudioConnected={null}
                playSoundOnCapture={false}
                captureAudio={false}
                keepAudioSession={false}
                // VIDEO // all disabled
                onRecordingStart={null}
                onRecordingEnd={null}
                // BARCODE // all disabled
                barCodeTypes={undefined}
                googleVisionBarcodeType={undefined}
                googleVisionBarcodeMode={undefined}
                onBarCodeRead={null}
                onGoogleVisionBarcodesDetected={null}
                // FACE DETECTION // all disabled
                onFaceDetected={null}
                onFacesDetected={null}
                onFaceDetectionError={null}
                faceDetectionMode={undefined}
                faceDetectionLandmarks={undefined}
                faceDetectionClassifications={undefined}
                trackingEnabled={undefined}
                // TEXT RECOGNIZE // all disabled
                onTextRecognized={null}
            />
        }
    }

    destroyCameraComponent = () => {
        InteractionManager.runAfterInteractions(() => {
            if (this.cameraComponentObj) {
                this.onCapture = undefined
                this.onCancel = undefined

                this.visible = false
                this.isCameraReady = false
                this.isTakingPicture = false

                this.cameraComponentObj = undefined
                this.cameraComponentRef = undefined
                console.debug('UICamera - Destroy')
            }

            // -------------------------------------------------------------------------------
            // Call Garbage Collector it's not necessary but we include it in the code to cover all possibilities during the tests
            DebugHelper.callGarbageCollector()
            // -------------------------------------------------------------------------------
        })

    }

    // -------------------------------------------------------------------------------

    handleButtonTakePicture() {
        InteractionManager.runAfterInteractions(() => {
            if (this.cameraComponentRef) {
                this.takePicture()
            }
        })
    }

    takePicture = async() => {
        if (this.cameraComponentRef) {
            this.isTakingPicture = true
            console.debug('UICamera - Take Picture: Start')

            const options = {
                quality: 0.7,
                base64: true,
                doNotSave: true,
                width: 480,
                exif: false,
                mirrorImage: false,
            }

            // -------------------------------------------------------------------------------

            let newImageData = undefined
            try {
                newImageData = await this.cameraComponentRef.takePictureAsync(options)
            } catch (error) {
                console.debug('UICamera - Error to capture image', jsonStringify(error))
                this.finishPictureCapture(undefined)
                return
            }

            if (newImageData == undefined) {
                this.finishPictureCapture(undefined)
                return
            }

            // -------------------------------------------------------------------------------

            // Base64
            let finalImageBase64Object = {uri: `data:image/png;base64,${newImageData.base64}`}
            console.debug('UICamera - base64', finalImageBase64Object)

            newImageData = undefined

            this.finishPictureCapture(finalImageBase64Object)
            // -------------------------------------------------------------------------------
        }
    }

    finishPictureCapture = (newImageBase64Object) => {
        console.debug('UICamera - Take Picture: Finish')

        if (this.onCapture != undefined) {
            this.onCapture(newImageBase64Object)
        }

        this.destroyCameraComponent()
    }

    // -------------------------------------------------------------------------------

    handleButtonCancel = () => {
        console.debug('UICamera - Take Picture: Cancel')
        if (this.onCancel) {
            this.onCancel()
        }

        this.destroyCameraComponent()
    }

    // -------------------------------------------------------------------------------

    renderButtons = () => {
        return <View style={styles.bottomBar}>
            <Button
                key={'keyButtonCancel'}
                title={'Cancel'}
                color={'#E33A47'}
                onPress={() => this.handleButtonCancel()}
            />

            <Button
                key={'keyButtonCapturePhoto'}
                title={'Take Picture'}
                color='#27AE60'
                active={this.isCameraReady == true && this.isTakingPicture == false}
                onPress={() => this.handleButtonTakePicture()}
            />
        </View>
    }

    render() {

        return (
            <Modal
                animationType={'slide'}
                transparent={true}
                visible={this.visible}
                onRequestClose={() => { }}
            >
                <View style={styles.view}>
                    {this.cameraComponentObj}
                    {this.renderButtons()}
                </View>
            </Modal >
        )
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    preview: {
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center',
        backgroundColor: '#358CBF'
    },
    bottomBar: {
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 0,
        left: 0,
        right: 0,
        maxHeight: 50,
        zIndex: 10,
        position: 'absolute'
    }
})