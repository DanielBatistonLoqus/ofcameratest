import { observable } from 'mobx'
import { observer } from 'mobx-react/native'
import { StyleSheet, StatusBar, View, Button, Alert } from 'react-native'
import React from 'react'

import DebugHelper from './utils/DebugHelper'
import UICameraTest from './components/UICameraTest'

@observer
class AppMain extends React.Component {
    @observable isCameraOpen = false

    constructor(props) {
        super(props)
        console.debug('App Starts')
    }

    // -------------------------------------------------------------------------------

    didCapture = (base64) => {
        this.isCameraOpen = false
        console.debug('Playground - didCapture')
    }

    didCancel = () => {
        this.isCameraOpen = false
        console.debug('Playground - didCancel')
    }

    // -------------------------------------------------------------------------------

    startMemoryMonitor = () => {
        // log available memory in Logcat each 2 seconds (Native function)
        DebugHelper.startDeviceMemoryMonitor()
    }

    renderSeparator = () => {
        return <View style={{ margin: 50 }} />
    }

    // -------------------------------------------------------------------------------
    render() {
        return (
            <View style={styles.view}>
                <StatusBar backgroundColor={'black'} translucent barStyle='light-content' />

                {this.isCameraOpen == true &&
                  <UICameraTest
                      key={'keyCamera'}
                      visible={this.isCameraOpen}
                      onCancel={this.didCancel}
                      onCapture={this.didCapture}
                  />
                }

                <Button
                    key={'keyButtonOpenCamera'}
                    title='Open Camera'
                    color='#27AE60'
                    onPress={() => {
                        this.isCameraOpen = true
                    }}
                />

                {this.renderSeparator()}

                <Button
                    key={'keyButtonDisplayMemory'}
                    title='Display Available Memory (Logcat)'
                    color='#27AE60'
                    onPress={() => {
                        Alert.alert(
                            'Warning',
                            'Get Memory Data from android device can also consume some memory.\nTo finish this process kill the app'
                        )
                        this.startMemoryMonitor()
                    }}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        marginTop: 0,
        backgroundColor: '#006AAE',
        marginBottom: 0,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    }
})

export default AppMain
