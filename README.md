# React-Native Camera Test for Zebra devices

--- 

## React-Native: Dependencies

guide: https://reactnative.dev/docs/environment-setup
- Node
- Watchman
- Java Development Kit
- Android Studio

---

# Camera Test
Run all commands on root project folder:

## Install packages
```npm install```

### Run app in Release Mode:
```react-native run-android --variant=release```

### Run app in Debug Mode:
```react-native run-android```


## APK
You can find a compiled APK on compiled_apk folder