package com.cameratestproject.util;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

public class DebugUtils {

    private static DecimalFormat twoDecimalForm = new DecimalFormat("#.##");
    private static JSONObject jsonObject = new JSONObject();
    private static ActivityManager activityManager;

    public static void initDebugUtils(Context context) {
        activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    }

    private static String convertToHumanizedMemorySize(long memoryBytes) {
        double mb = memoryBytes / 1048576.0;
        return twoDecimalForm.format(mb).concat(" MB");
    }

    public static String getDeviceMemoryData(Context context) {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);

        String totalMemoryUsage = convertToHumanizedMemorySize(memoryInfo.totalMem);
        String availableMemory = convertToHumanizedMemorySize(memoryInfo.availMem);
        String threshold = convertToHumanizedMemorySize(memoryInfo.threshold);
        boolean isLowMemory = memoryInfo.lowMemory;

        try {
            jsonObject.put("totalMem", totalMemoryUsage);
            jsonObject.put("availMem", availableMemory);
            jsonObject.put("threshold", threshold);
            jsonObject.put("isLowMemory", isLowMemory);
            jsonObject.put("currentTimestamp", System.currentTimeMillis());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
        // return "{\"totalMem\":\"3733.59 MB\",\"availMem\":\"1857.04 MB\",\"threshold\":\"216 MB\",\"isLowMemory\":false,\"currentTimestamp\":1588589847060}";
    }

    public static void startDeviceMemoryMonitor(final Context context) {
        final Handler handler = new Handler();
        final int delay = 2000; //milliseconds
        handler.postDelayed(new Runnable() {
            public void run() {
                logDeviceMemoryData(context);
                handler.postDelayed(this, delay);
            }
        }, delay);
    }

    public static void logDeviceMemoryData(Context context) {
        Log.d("MEMORY", getDeviceMemoryData(context));
    }
    
    public static String getDeviceInfo() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("android", android.os.Build.VERSION.RELEASE + "(" + android.os.Build.VERSION.SDK_INT + ")");
            jsonObject.put("device", android.os.Build.DEVICE );
            jsonObject.put("model", android.os.Build.MODEL);
            jsonObject.put("display", android.os.Build.DISPLAY);
            jsonObject.put("brand", android.os.Build.BRAND);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }
}
