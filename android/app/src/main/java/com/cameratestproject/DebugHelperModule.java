package com.cameratestproject;

import android.widget.Toast;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import com.cameratestproject.util.DebugUtils;

import java.util.Map;
import java.util.HashMap;

public class DebugHelperModule extends ReactContextBaseJavaModule {
  private static ReactApplicationContext reactContext;

  private static final String DURATION_SHORT_KEY = "SHORT";
  private static final String DURATION_LONG_KEY = "LONG";

  DebugHelperModule(ReactApplicationContext context) {
      super(context);
      reactContext = context;

      DebugUtils.initDebugUtils(getReactApplicationContext());
  }
  
  @Override
  public String getName() {
      return "DebugHelper";
  }
  
  @Override
  public Map<String, Object> getConstants() {
      final Map<String, Object> constants = new HashMap<>();
      constants.put(DURATION_SHORT_KEY, Toast.LENGTH_SHORT);
      constants.put(DURATION_LONG_KEY, Toast.LENGTH_LONG);
      return constants;
  }
  
  @ReactMethod
  public void show(String message, int duration) {
      Toast.makeText(getReactApplicationContext(), message, duration).show();
  }

  @ReactMethod
  public void getDeviceMemoryData(Callback callback) {
      callback.invoke(DebugUtils.getDeviceMemoryData(getReactApplicationContext()));
  }
  
  @ReactMethod
  public void getDeviceInfo(Callback callback) {
      callback.invoke(DebugUtils.getDeviceInfo());
  }

  @ReactMethod
  public void logDeviceMemoryData() {
      DebugUtils.logDeviceMemoryData(getReactApplicationContext());
  }

  @ReactMethod
  public void startDeviceMemoryMonitor() {
      DebugUtils.startDeviceMemoryMonitor(getReactApplicationContext());
  }

  @ReactMethod
  public void callGarbageCollector() {
      Runtime.getRuntime().gc();
  }

}